<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Vb_Minisites
 * @subpackage Vb_Minisites/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Vb_Minisites
 * @subpackage Vb_Minisites/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Vb_Minisites
 * @subpackage Vb_Minisites/admin
 * @author     Lucio Crusca <info@virtualbit.it>
 */
class Vb_Minisites_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private static $options_group;
	private static $plugin_options_slug;
  
	public static $url_of_logo_to_be_replaced;
	public static $mini_slugs;
	public static $mini_home_logos;
	public static $mini_children_logos;
	public static $mini_home_sticky_logos;
	public static $mini_children_sticky_logos;
	public static $number_of_minisites;
	public static $accordion_classes;


	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		self::$url_of_logo_to_be_replaced = $this->plugin_name . '_vbms_uoltbr'; 
		self::$options_group = $this->plugin_name . '_vbms_options';
		self::$mini_slugs = array();
		self::$mini_home_logos = array();
		self::$mini_children_logos = array();
		self::$mini_home_sticky_logos = array();
		self::$mini_children_sticky_logos = array();
		self::$number_of_minisites = 4;
		self::$accordion_classes = $this->plugin_name . '_vbms_accordion_classes';
	    for ($i = 0; $i < self::$number_of_minisites; $i++) {
		  array_push(self::$mini_slugs, $this->plugin_name . '_vbms_minisite_slug_' . $i);
		  array_push(self::$mini_home_logos, $this->plugin_name . '_vbms_homelogo_' . $i);
		  array_push(self::$mini_children_logos, $this->plugin_name . '_vbms_childpages_logo_' . $i);
		  array_push(self::$mini_home_sticky_logos, $this->plugin_name . '_vbms_homesticky_' . $i);
		  array_push(self::$mini_children_sticky_logos, $this->plugin_name . '_vbms_childrpages_sticky_' . $i);
		}
		
		self::$plugin_options_slug = $this->plugin_name . '-admin-options';	
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Vb_Minisites_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Vb_Minisites_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/vb-minisites-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Vb_Minisites_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Vb_Minisites_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/vb-minisites-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function register_settings() 
  	{
	  for ($i = 0; $i < self::$number_of_minisites; $i++)
	  {
		register_setting(self::$plugin_options_slug, self::$mini_slugs[$i]);
		register_setting(self::$plugin_options_slug, self::$mini_home_logos[$i]);
		register_setting(self::$plugin_options_slug, self::$mini_children_logos[$i]);
		register_setting(self::$plugin_options_slug, self::$mini_home_sticky_logos[$i]);
		register_setting(self::$plugin_options_slug, self::$mini_children_sticky_logos[$i]);
      }
	  register_setting(self::$plugin_options_slug, self::$accordion_classes);
	  register_setting(self::$plugin_options_slug, self::$url_of_logo_to_be_replaced);
	  add_settings_section(self::$plugin_options_slug, "Slugs and logos", array($this, "settings_section_title"), self::$plugin_options_slug);
	   
	  $callable_for_logo_url = function () {
		$this->show_input(self::$url_of_logo_to_be_replaced, "", 
    	    'Enter the URL of the logo image to be replaced in Minisites pages.', 
	        false, 'vbms-settings-url');
	  };

	  add_settings_field(self::$url_of_logo_to_be_replaced, 
	  					"Main Logo URL", 
						$callable_for_logo_url, self::$plugin_options_slug, self::$plugin_options_slug);

	  for ($i = 0; $i < self::$number_of_minisites; $i++)
	  {
		$callable_for_ith_slug = function () use ($i) {
		  $this->show_slug_input($i, Vb_Minisites_Admin::$mini_slugs[$i], "", 
				'Enter the slug of the minisite page that is parent of all other minisite pages. This must be unique.', 
				false, 'vbms-settings-slug');
		};

		add_settings_field(self::$mini_slugs[$i], 
							"Minisite ".($i + 1)." slug", 
							$callable_for_ith_slug, 
							self::$plugin_options_slug, self::$plugin_options_slug);

		$callable_for_ith_homelogo = function () use ($i) {
			$this->show_slug_input($i, Vb_Minisites_Admin::$mini_home_logos[$i], "", 
					'Enter the URL of the logo image for the minisite homepage', false, 'vbms-settings-url');
		};

		add_settings_field(self::$mini_home_logos[$i], 
							"Minisite ".($i + 1)." homepage logo", 
							$callable_for_ith_homelogo, 
							self::$plugin_options_slug, self::$plugin_options_slug);

		$callable_for_ith_homestickylogo = function () use ($i) {
			$this->show_slug_input($i, Vb_Minisites_Admin::$mini_home_sticky_logos[$i], "", 
			'Enter the URL of the sticky logo image for the minisite homepage', false, 'vbms-settings-url');
		};

		add_settings_field(self::$mini_home_sticky_logos[$i], 
							"Minisite ".($i + 1)." homepage sticky logo", 
							$callable_for_ith_homestickylogo, 
							self::$plugin_options_slug, self::$plugin_options_slug);

		$callable_for_ith_childrenlogo = function () use ($i) {
			$this->show_slug_input($i, Vb_Minisites_Admin::$mini_children_logos[$i], "", 
			'Enter the URL of the logo image for the minisite children pages', false, 'vbms-settings-url');
		};

		add_settings_field(self::$mini_children_logos[$i], 
							"Minisite ".($i + 1)." children pages logo", 
							$callable_for_ith_childrenlogo, 
							self::$plugin_options_slug, self::$plugin_options_slug);
		
		$callable_for_ith_childrenstickylogo = function () use ($i) {
			$this->show_slug_input($i, Vb_Minisites_Admin::$mini_children_sticky_logos[$i], "", 
			'Enter the URL of the sticky logo image for the minisite children pages', false, 'vbms-settings-url');
		};

		add_settings_field(self::$mini_children_sticky_logos[$i], 
							"Minisite ".($i + 1)." children pages sticky logo", 
							$callable_for_ith_childrenstickylogo, 
							self::$plugin_options_slug, self::$plugin_options_slug);
		}
		
		$callable_for_accordion_classes = function () {
		$this->show_input(self::$accordion_classes, "", 
		'Comma separated list of CSS classes to apply VBMS Accordion script to (see documentation)', false, 'vbms-settings-css-classes');
		};

		add_settings_field(self::$accordion_classes, 
						"VMBS accordion CSS classes", 
						$callable_for_accordion_classes, 
						self::$plugin_options_slug, self::$plugin_options_slug);

	}
	
	public function menu() 
	{
	  add_options_page(self::$plugin_options_slug, 'Minisites', 'manage_options', self::$plugin_options_slug, array($this, 'show_options'));
	}
	
	public function settings_section_title() 
	{
	  echo "<h5>".__("Set the slug for each minisite homepage. Slugs must be unique. ", "vb-minisites")."</h5>";
	}
	  
	public function option($option_name, $default, $allowZero = false)
	{
	  $val = get_option($option_name, false);
	  if (empty($val) && (($allowZero === false && trim($val) === "0") || trim($val) === ""))
	  {
		update_option($option_name, $default);
		return $default;
	  }
	  return $val;
	}

	public function show_slug_input($input_num, 
									$option_name, 
									$default = "", $note = "", $allowZero = false, $clazz = "")
	{
      ?>
      <input type="text" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" class="<?php echo $clazz; ?>" size="20" value="<?php echo $this->option($option_name, $default, $allowZero); ?>"/>
	  <?php 
	  if (!empty($note)) echo '<span class="vbms-admin-options-note vbms-admin-options-note-'.$option_name.'">'.$note.'</span>';		   
	}

	public function show_input($option_name, 
									$default = "", $note = "", $allowZero = false, $clazz = "")
	{
      ?>
      <input type="text" id="<?php echo $option_name; ?>" name="<?php echo $option_name; ?>" class="<?php echo $clazz; ?>" size="20" value="<?php echo $this->option($option_name, $default, $allowZero); ?>"/>
	  <?php 
	  if (!empty($note)) echo '<span class="vbms-admin-options-note vbms-admin-options-note-'.$option_name.'">'.$note.'</span>';		   
	}

	public function show_options() {
		?>
		<div class="wrap">
		  <h2>Vb Minisites</h2>
		  <form method="post" action="options.php">
		<?php
		settings_fields(self::$plugin_options_slug);
		do_settings_sections(self::$plugin_options_slug);
		submit_button();
		echo "</form></div>";
	  }
	

	public static function getMainLogoUrl() {
	  return get_option(self::$url_of_logo_to_be_replaced, false);	
	}

	public static function getMinisiteSlug($msIndex) {
	  return get_option(self::$mini_slugs[$msIndex], false);
	}

	public static function getMinisiteHomepageHeadLogo($msIndex) {
		return get_option(self::$mini_home_logos[$msIndex], false);
	}

	public static function getMinisiteChildrenPagesHeadLogo($msIndex) {
		return get_option(self::$mini_children_logos[$msIndex], false);
	}

	public static function getMinisiteHomepageStickyLogo($msIndex) {
		return get_option(self::$mini_home_sticky_logos[$msIndex], false);
	}

	public static function getMinisiteChildrenPagesStickyLogo($msIndex) {
		return get_option(self::$mini_children_sticky_logos[$msIndex], false);
	}

	public static function getAccordionClasses() {
		$classes = get_option(self::$accordion_classes, false);
		$result = explode(',', $classes);
		return $result;
	}
  }

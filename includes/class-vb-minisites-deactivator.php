<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Vb_Minisites
 * @subpackage Vb_Minisites/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Vb_Minisites
 * @subpackage Vb_Minisites/includes
 * @author     Lucio Crusca <info@virtualbit.it>
 */
class Vb_Minisites_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

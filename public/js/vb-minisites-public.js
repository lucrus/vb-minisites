(function( $ ) {
  'use strict';
  
    function slideAccordion(jqThis, event, fargs, hideOthers = true) {
      var aclazz = fargs[0];
      var thisID = jqThis.attr('id');
      if (hideOthers === true) {
        hideAccordion(jqThis, event, fargs, thisID);
      }
      var thisIDSel = '#' + thisID;
      var thisIDClassSel = '.' + thisID;
      var toggleTargetID =  thisIDSel + '-' + aclazz;
      $(toggleTargetID).slideToggle();

      var toggleTargetClass = thisIDClassSel + '-' + aclazz;
      $(toggleTargetClass).slideToggle();

      var iconClass = aclazz + '-icon';
      var clickedClass = iconClass + '-clicked';
      var iconTarget = thisIDSel + ' ' + '.' + iconClass;
      $(iconTarget).toggleClass(clickedClass);
    }

    function hideAccordion(jqThis, event, fargs, idToExclude = null) {
      var aclazz = fargs[0];
      $('.' + aclazz).each(function () {
        var curID = $(this).attr('id');
        if (curID !== idToExclude) { 
          var hideTargetByID = $('#' + curID + '-' + aclazz);
          if (hideTargetByID.is(':visible')) {
            slideAccordion($(this), event, fargs, false);
          }
          var hideTargetByClass = $('.' + curID + '-' + aclazz);
          if (hideTargetByClass.is(':visible')) {
            slideAccordion($(this), event, fargs, false);
          }
          if (event === 'load') { 
            $('#' + curID + ' .' + aclazz + '-icon').removeClass(aclazz + '-icon-clicked');
          }
        }
      });
    }

    function parseEventsMappings(events)
    {
      $.each(events, function (index, value)
      {
        var target = value[0];
        if (target === 'public')
        {
          var event = value[1];
          var fname = value[2];
          var fargs = value[3];
          if (event === 'load')
            eval(fname + '($(this), "load", fargs);');
          else
          {
            var cssclasstoselect = fargs[0];
            $('.' + cssclasstoselect).on(event, function (e) {
              return eval(fname + '($(this), e, fargs);');
            });
          }
        }
      });
    }

    function bindStaticEvents() {
      $('.advantec-accordion-item').click(function (ev) {
        var jqThis = $(this);
        jqThis.css('cursor', 'pointer');
        var metaLink = jqThis.find('.advantec-accordion-item-metalink');
        var hlink = metaLink.html();
        window.location.href = hlink;
      });
      var maxh = Math.max.apply(null, $(".advantec-accordion-con-immagini").map(function ()
      {
          return $(this).height();
      }).get());
      // $(".advantec-accordion-con-immagini").css('min-height', maxh + 'px');
    }

  /*
   * The ready function parses the jshooks_params array received from the server
   */
    $(document).ready(function ()
    {
      parseEventsMappings(jshooks_params.events);

      bindStaticEvents();
    });

    $(document).ajaxStart(function () {
      $('body').css({'cursor': 'wait'});
    }).ajaxStop(function () {
      $('body').css({'cursor': 'default'});
    });
	
})( jQuery );

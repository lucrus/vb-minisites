<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://virtualbit.it/
 * @since      1.0.0
 *
 * @package    Vb_Minisites
 * @subpackage Vb_Minisites/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Vb_Minisites
 * @subpackage Vb_Minisites/includes
 * @author     Lucio Crusca <info@virtualbit.it>
 */
class Vb_Minisites {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Vb_Minisites_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * The admin instance
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Vb_Minisites_Admin    $plugin_admin    The admin instance
	 */
	protected $plugin_admin;	

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'VB_MINISITES_VERSION' ) ) {
			$this->version = VB_MINISITES_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'vb-minisites';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Vb_Minisites_Loader. Orchestrates the hooks of the plugin.
	 * - Vb_Minisites_i18n. Defines internationalization functionality.
	 * - Vb_Minisites_Admin. Defines all hooks for the admin area.
	 * - Vb_Minisites_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-vb-minisites-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-vb-minisites-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-vb-minisites-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-vb-minisites-public.php';

		$this->loader = new Vb_Minisites_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Vb_Minisites_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Vb_Minisites_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$this->plugin_admin = new Vb_Minisites_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $this->plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $this->plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action('admin_init', $this->plugin_admin, 'register_settings');
		$this->loader->add_action('admin_menu', $this->plugin_admin, 'menu');
	
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Vb_Minisites_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
		add_filter("wiz_replace_header_logo", array($this, 'wiz4_replace_logo'));
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Vb_Minisites_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	public function uri_explode($uri) {
		$array = explode('/', $uri);
		$filtered = array_filter($array, function ($v) {
			return !empty($v);
		} );
		return array_values($filtered);
	}

	public function is_minisite_homepage($id = null)
	{
	  if (empty($id)) {
		$uri_arr = $this->uri_explode($_SERVER['REQUEST_URI']);
		if (sizeof($uri_arr) != 1)
		  return false;
		$uri = $uri_arr[0];
	  } else {
		if (is_string($id)) {
		  $uri_arr = $this->uri_explode($id);
		  if (sizeof($uri_arr) != 1)
		    return false;
		  $uri = $uri_arr[0];
		} else {
		  $uri = get_post($id)->post_name;
		}
	  }
	
	  $minisitehome = false;
	  for ($i = 0; $i < Vb_Minisites_Admin::$number_of_minisites; $i++) {
		  $mslug = Vb_Minisites_Admin::getMinisiteSlug($i);
		  $minisitehome = $minisitehome || ($uri == $mslug);
	  }
	  
	  return $minisitehome;
	}
	
	/**
	 * This function assumes WP is NOT installed in a subdirectory. 
	 * If it is, then you aren't supposed to use this plugin, because the
	 * settings page do not support entering the subdirectory name anyway.
	 */
	function get_minisite_homepage_uri($uri = null)
	{
	  if (is_null($uri))
	    $uri = $_SERVER['REQUEST_URI'];
	  $uri_arr = $this->uri_explode($uri);
	  if (sizeof($uri_arr) === 0)
		return '/'; // no subdirectory, remember?
		
	  if ($this->is_minisite_homepage($uri_arr[0]))
		return '/'.$uri_arr[0];
		
	  return '/'; // we are not in a configured minisite, so we return the main page URI
	}
	
	function is_minisite_child_page($uri = null)
	{
	  if (is_null($uri))
	    $uri = $_SERVER['REQUEST_URI'];
	  $uri_arr = $this->uri_explode($uri);
	  if (sizeof($uri_arr) < 2)
		return false;
	  return $this->is_minisite_homepage($uri_arr[0]);
	}

	function is_minisite_page($uri = null) {
		return $this->is_minisite_child_page($uri) || $this->is_minisite_homepage($uri);
	}

	function get_minisite_index() {
	  if ($this->is_minisite_page()) {
		  $huri = $this->uri_explode($this->get_minisite_homepage_uri())[0];
		  for ($i = 0; $i < Vb_Minisites_Admin::$number_of_minisites; $i++) {
			  if ($huri === Vb_Minisites_Admin::getMinisiteSlug($i))
			    return $i;
		  }
	  }
	  return false;
	}

    /**
	 * recupera l'url del logo per tutte le pagine dei minisiti. Va a leggere 
     * l'URL nella configurazione del plugin.
	 * L'argomento può valere 'head' (default) per il logo principale, o
	 * 'sticky' per il logo del menù sticky.
     * Se la pagina attuale non è di un minisito, ritorna false */
    function get_logo_url($logotype = 'head')
    {
	  if ($this->is_minisite_homepage())
	  {
		if ($logotype === 'head')
			return Vb_Minisites_Admin::getMinisiteHomepageHeadLogo($this->get_minisite_index());
		else
			return Vb_Minisites_Admin::getMinisiteHomepageStickyLogo($this->get_minisite_index());
	  }
	  if ($this->is_minisite_child_page()) {
		if ($logotype === 'head')
			return Vb_Minisites_Admin::getMinisiteChildrenPagesHeadLogo($this->get_minisite_index());
		else
			return Vb_Minisites_Admin::getMinisiteChildrenPagesStickyLogo($this->get_minisite_index());		  
	  }

      return false;
	}

	public function enqueue_scripts() {
		$script_handle = 'vbms-scripts';
		$jsfileurl = plugin_dir_url(__FILE__) . '../public/js/vb-minisites-public.js';
		wp_register_script($script_handle, $jsfileurl, array('jquery'));
	
		$accordionClasses = Vb_Minisites_Admin::getAccordionClasses();
		$hooks_data_events = [];
		foreach ($accordionClasses as $aclazz) {
		  if (empty($aclazz)) continue;
		  array_push($hooks_data_events, // each element is a JS callback either fot 'public' site or 'admin'
					  array('public', // we need only public site JS here
					        'click', // the JS event to hook
							'slideAccordion', // the JS function name to call on said event
							array($aclazz))); // the parameters for that function, where the 1st one MUST be a CSS class name to select as `this`
		  array_push($hooks_data_events, // each element is a JS callback either fot 'public' site or 'admin'
					  array('public', // we need only public site JS here
					        'load', // the JS event to hook
							'hideAccordion', // the JS function name to call on said event
							array($aclazz))); // the parameters for that function, where the 1st one MUST be a CSS class name to select as `this`
		}
		$hooks_data = array('events' => $hooks_data_events);
	
		wp_localize_script($script_handle, "jshooks_params", $hooks_data);
		wp_enqueue_script($script_handle);
	
	}

	public function wiz4_replace_logo($image)
	{
		if ($this->is_minisite_page()) {
			$mainLogoUrl = Vb_Minisites_Admin::getMainLogoUrl();
			if (strcmp($mainLogoUrl, $image[0]) === 0) {
				$image[0] = $this->get_logo_url();
				list($width, $height, $type, $attr) = getimagesize($image[0]);
				$image[1] = $width;
				$image[2] = $height;
				}
		}
		return $image;
	}
}

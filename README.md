This plugin provides some options that may be useful when you host a few pages inside your site that logically compose a minisite of some partner of yours.

The well known WordPress Multisite configuration is a totally different thing, it has nothing to do with this plugin and you can use both, if you want (except this plugin has not been tested in multisite enviromnents, so YMMV).


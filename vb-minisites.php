<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://virtualbit.it/
 * @since             1.0.0
 * @package           Vb_Minisites
 *
 * @wordpress-plugin
 * Plugin Name:       VB Minisites
 * Plugin URI:        https://gitlab.com/lucrus/vb-minisites
 * Description:       Options to set custom logos on minisite pages (only for Leap13 Wiz theme) and accordions.
 * Version:           1.0.4
 * Author:            Lucio Crusca
 * Author URI:        https://virtualbit.it/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       vb-minisites
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'VB_MINISITES_VERSION', '1.0.4' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-vb-minisites-activator.php
 */
function activate_vb_minisites() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-vb-minisites-activator.php';
	Vb_Minisites_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-vb-minisites-deactivator.php
 */
function deactivate_vb_minisites() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-vb-minisites-deactivator.php';
	Vb_Minisites_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_vb_minisites' );
register_deactivation_hook( __FILE__, 'deactivate_vb_minisites' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-vb-minisites.php';

$vb_minisites_plugin_instance = new Vb_Minisites();
$vb_minisites_plugin_instance->run();


/* definisco la ot_get_option che è una funzione del tema Wiz, così il tema
 * la trova già definita ed usa questa invece di quella sua originale.
 * Questa è una copia di quella originale, ma in più cambia il logo
 * per i minisiti, solo nel front-end. Per tutto il resto fa la stessa
 * cosa della ot_get_option originale nella versione 3.2 di Wiz, sperando
 * che in versioni future non cambino la logica di questa funzione (che comunque
 * è abbastanza semplice e basilare) */
function ot_get_option( $option_id, $default = '' ) {

  // inizio codice aggiunto da me (Lucio)
  //
  //
	global $vb_minisites_plugin_instance;
    $is_minisite_page = $vb_minisites_plugin_instance->is_minisite_page();
    if (!is_admin() && $is_minisite_page === true)
    {
      switch ($option_id) {
        case 'sticky_logo':
          $minisite_logo = $vb_minisites_plugin_instance->get_logo_url('sticky');
          break;
        case 'logo':
          $minisite_logo = $vb_minisites_plugin_instance->get_logo_url('head');
          break;
      }
      if (!empty($minisite_logo))
        return  $minisite_logo;
    }
  //
  //
  // Fine codice aggiunto da me (Lucio)
  //
  //
  // Da qui in avanti è una copia della funzione originale di Wiz
  //
  //

  /* get the saved options */  
    $options = get_option( ot_options_id() );
    
  /* look for the saved value */ 
    if ( isset( $options[$option_id] ) && '' != $options[$option_id] ) {
        
      return ot_wpml_filter( $options, $option_id );
      
    }
    
    return $default;
  
}

function _vbit_advantec_home_url() {
	global $vb_minisites_plugin_instance;
  
  $uri = $_SERVER['REQUEST_URI'];
	if ($vb_minisites_plugin_instance->is_minisite_page($uri)) {
		return $vb_minisites_plugin_instance->get_minisite_homepage_uri($uri);
	}
	else
	{
		return home_url();
	}
  
}  

function _vbit_advantec_is_minisite_page_or_post($minisite_slug) {
  $current_slug = _vbit_advantec_home_url();
  $homepos = strpos($current_slug, $minisite_slug);
  $belongsToMinisite = ($homepos !== false) && (is_numeric($homepos)) && ($homepos >= 0);
  return $belongsToMinisite;
}

function _vbit_advantec_adminfavicon()
{
  echo '<link rel="icon" type="image/x-icon" href="/favicon-admin.ico" />';
}
add_action( 'admin_head', '_vbit_advantec_adminfavicon' );
